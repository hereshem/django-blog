from django.shortcuts import render

# Create your views here.
from blog.models import Article


def index(req):
    blogs = Article.objects.all()
    dict = {"data": blogs, "count": len(blogs)}
    return render(req, "blog/index.html", dict)

def detail(req, id):
    article = Article.objects.get(id=id)
    dict = {"data": article}
    return render(req, "blog/detail.html", dict)

def comment(req, id):
    ""