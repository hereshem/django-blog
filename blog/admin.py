from django.contrib import admin

# Register your models here.

from blog.models import Author, Article, Category

class ArticleAdmin(admin.ModelAdmin):
    list_display = ["title", "author","created", "category", "published"]
    list_filter = ["title", "created"]
    search_fields = ["title", "description"]

admin.site.register(Article, ArticleAdmin)
admin.site.register(Author)
admin.site.register(Category)
